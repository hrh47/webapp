# Select a base image
FROM node:alpine

# Run some commands and configurations
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
EXPOSE 8080

# Set startup commands
CMD [ "yarn", "start" ]
